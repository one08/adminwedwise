import { Component, OnInit } from "@angular/core";
import { FirebaseService } from "../../services/firebase.service";
import { Router, ActivatedRoute, Params } from "@angular/router";

@Component({
  selector: "app-edit-vendor",
  templateUrl: "./edit-vendor.component.html",
  styleUrls: ["./edit-vendor.component.css"]
})
export class EditVendorComponent implements OnInit {
  id: any;
  firstname: any;
  lastname: any;
  email: any;
  password: any;
  restaurant_name: any;
  public restaurants: any;

  constructor(
    private firebaseService: FirebaseService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.firebaseService.getAvailableRestaurants().valueChanges().subscribe(res => {
      this.restaurants = res;
    });

    this.id = this.route.snapshot.params["id"];
    console.log("this vendor id is =>", this.id);
    this.firebaseService.getVendorDetails(this.id).valueChanges().subscribe(item => {
      console.log("i want to know items", item);
      this.firstname = item.displayName;
      this.lastname = item.lastName;
      this.email = item.email;
      this.password = "********";
    });
  }
  onEditVendor() {
    let vendor = {
      displayName: this.firstname,
      lastName: this.lastname,
      email: this.email,
      // password: this.password
      facebook: false,
      is_vendor: true,
      photoURL: "assets/images/person.png"
    };
    console.log("this is vendor data", vendor);
    this.firebaseService.updateVendor(this.id, vendor);
    this.router.navigate(["/vendor"]);
  }
}
