import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { FirebaseService } from "../../services/firebase.service";

@Component({
  selector: "app-add-vendor",
  templateUrl: "./add-vendor.component.html",
  styleUrls: ["./add-vendor.component.css"]
})
export class AddVendorComponent implements OnInit {
  public form: FormGroup;
  public restaurants: any;

  constructor(
    private fb: FormBuilder,
    private firebaseService: FirebaseService
  ) {}

  ngOnInit() {
    this.form = this.fb.group({
      FirstName: ["", Validators.required],
      LastName: ["", Validators.required],
      EmailAddress: ["", Validators.required],
      Password: ["", Validators.required],
      Restaurant: ["", Validators.required],
      Status: [1, Validators.required]
    });

    this.firebaseService.getAvailableRestaurants().valueChanges().subscribe(res => {
      this.restaurants = res;
    });
  }

  createVendor() {
    if (!this.form.valid) {
      alert(
        "All fields are mandatory. Please fill in all the values correctly"
      );
    } else {
      console.log("Ready to create");
      let data = this.form.value;
      let status = this.firebaseService
        .addVendor(data)
        .then(res => {
          this.form.reset();
        })
        .catch(e => {
          console.log(e);
          alert(e);
        });
      console.log(status);
    }
  }
}
