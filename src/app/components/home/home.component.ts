import { Component, OnInit } from '@angular/core';

import {FlashMessagesService} from 'angular2-flash-messages';
import { AuthService } from 'app/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
	public authService:AuthService,
	public flashMessage:FlashMessagesService
  ) { }

  ngOnInit() {
  }
  
  login(){
	  this.authService.googleLogin);
	  
  }

}
