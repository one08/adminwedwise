import { Component, OnInit } from "@angular/core";
import { FirebaseService } from "../../services/firebase.service";

import { ValueTransformer } from "@angular/compiler/src/util";
import { Router, ActivatedRoute, Params } from "@angular/router";
import * as firebase from "firebase";

@Component({
  selector: "app-vendor",
  templateUrl: "./vendor.component.html",
  styleUrls: ["./vendor.component.css"]
})
export class VendorComponent implements OnInit {
	
	public vendors: any;
	private vendor: any;
  public loopData: any = ["displayName", "email"];

  constructor(
    private firebaseService: FirebaseService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
	
    this.firebaseService.getVendor().valueChanges().subscribe(res => {
			this.vendors = res;
    });
  }

  getName(vendor) {
    return vendor.displayName + " " + vendor.lastName;
  }
  getEmail(vendor) {
    return vendor.email;
  }
  onDeleteVendor(key: string) {
		if(confirm('Are you sure to delete this recode ?') == true){
			this.firebaseService.deleteVendor(key);
			
			this.router.navigate(["/vendor"]);
		}
    
  }
}
