import { Component, OnInit } from '@angular/core';
import { AngularFire, AngularFireAuth } from 'angularfire2';
import { FlashMessagesService} from 'angular2-flash-messages';
import {Router} from '@angular/router';
import { first, tap } from 'rxjs/operators';
import { AuthService } from 'app/services/auth.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  private isLoggedIn: Boolean;
  constructor(public authService:AuthService,
    public flashMessage: FlashMessagesService, public router: Router
 ) {

  

  }

  ngOnInit() {
  
  }

  login(){
  this.authService.googleLogin();
;	  
  }

  logout(){

    // this.af.auth.logout()
    // .then((res)=>{
    //   this.flashMessage.show('You are logged out',
    //   {cssClass: 'alert-success', timeout: 3000});
  
    //   this.router.navigate(['']);
    // });

	
  }
}
