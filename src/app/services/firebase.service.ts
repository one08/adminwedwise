import {Injectable} from '@angular/core';
import {AngularFireDatabase,AngularFireDatabaseModule,AngularFireList,AngularFireObject} from 'angularfire2/database';
import {AngularFireModule,FirebaseAuth} from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';


@Injectable()
export class FirebaseService {

    listings: AngularFireList<any[]>;
    listing: AngularFireObject<any>;

    restaurants: AngularFireList<any[]>;
    restaurant: AngularFireObject<any>;

    categories: AngularFireList<any[]>;
    category_details: AngularFireObject<any>;

    items: AngularFireList<any[]>;
    item_details: AngularFireObject<any>;

    orders: AngularFireList<any[]>;
    order_details: AngularFireObject<any>;

    vendors: AngularFireList<any[]>;
    vendor:AngularFireObject<any>;

    folder: any;
    itemFolder: any;
    restaurantFolder: any;
    categoryFolder: any;




    constructor(private db: AngularFireDatabase, public afh:AngularFireAuth) {
       
        this.listings = this.db.list('/listings') as AngularFireList<Listing[]>;
       
        this.restaurants = this.db.list('/restaurants') as AngularFireList<Listing[]>;
     
        this.categories = this.db.list('/category') as AngularFireList<Category[]>;
      
        this.items = this.db.list('/items') as AngularFireList<Item[]>;
       
        this.orders = this.db.list('/orders') as AngularFireList<Order[]>;
  
        this.vendors = this.db.list('/users') as AngularFireList<Vendor[]>;
        
        this.folder = 'listingimages';
        this.itemFolder = 'itemimages';
        this.restaurantFolder = 'restaurantimages';
        this.categoryFolder = 'categoryimages';
    }


    getRestaurants() {

        return this.restaurants;
    }

    getOrders() {

        return this.orders;
    }

    getOrderDetails(id) {

        //this.restaurant = this.db.object('users/' + uid + '/restaurants/'+id) as AngularFireObject<Restaurant>
        this.order_details = this.db.object('/orders/' + id) as AngularFireObject<Order>
        return this.order_details;

    }

    updateOrderStatus(id, order_details) {
        //private items = this.db.list('listings');
        //items.update(category);
        return this.orders.update(id, order_details);

    }

    addRestaurant(restaurant) {


        let storageRefItem = firebase.storage().ref();
        for (let selectedItemFile of [(<HTMLInputElement>document.getElementById('image')).files[0]]) {

            //let path = '/${this.folder}/${selectedFile.name}';
            let pathItem = `/${this.restaurantFolder}/${selectedItemFile.name}`;
            let iRefItem = storageRefItem.child(pathItem);
            iRefItem.put(selectedItemFile).then((snapshot) => {
                restaurant.image = pathItem;

                let storageRef = firebase.storage().ref();
                let spaceRef = storageRef.child(restaurant.image);

                console.log(restaurant.image);
                storageRef.child(restaurant.image).getDownloadURL().then((url) => {
                    // Set image url
                    console.log(url);


                    restaurant.firebase_url = url;

                    return this.restaurants.push(restaurant);

                }).catch((error) => {
                    console.log(error);
                });


                // return this.restaurants.push(restaurant);
            });
        }


    }


    getRestaurantDetails(id) {
        this.restaurant = this.db.object('/restaurants/' + id) as AngularFireObject<Restaurant>
        return this.restaurant;
    }

    getVendorDetails(id) {
        this.vendor = this.db.object('/users/' + id) as AngularFireObject<Vendor>
        return this.vendor;
    }

    deleteRestaurant(id) {
        //private items = this.db.database.list('listings');
        //items.remove(category);
        return this.restaurants.remove(id);

    }

    //delete vendor
    deleteVendor($key:string) {
        return this.vendors.remove($key);
    }


    updateRestaurant(id, restaurant) {
        //private items = this.db.database.list('listings');
        //items.update(category);
        return this.restaurants.update(id, restaurant);

    }

    //update vendor
    updateVendor(id, vendor) {
        return this.vendors.update(id, vendor);
    }


    getListings() {

        return this.listings;
    }

    getListingDetails(id) {
        this.listing = this.db.object('/listings/' + id) as AngularFireObject<Listing>
        return this.listing;
    }


    addListing(listing) {
        // Create root ref
        let storageRef = firebase.storage().ref();
        for (let selectedFile of [(<HTMLInputElement>document.getElementById('image')).files[0]]) {

            //let path = '/${this.folder}/${selectedFile.name}';
            let path = `/${this.folder}/${selectedFile.name}`;
            let iRef = storageRef.child(path);
            iRef.put(selectedFile).then((snapshot) => {
                listing.image = selectedFile.name;
                listing.path = path;
                return this.listings.push(listing);
            });
        }
    }

    updateListing(id, listing) {
        //private items = this.db.database.list('listings');
        //items.update(category);
        return this.listings.update(id, listing);

    }

    deleteListing(id) {
        //private items = this.db.database.list('listings');
        //items.remove(category);
        return this.listings.remove(id);

    }

    addCategory(category) {

        //private items = this.db.database.list('category');
        //items.push(category);

        let storageRefItem = firebase.storage().ref();
        
        for (let selectedItemFile of [(<HTMLInputElement>document.getElementById('image')).files[0]]) {

            //let path = '/${this.folder}/${selectedFile.name}';
            let pathItem = `/${this.categoryFolder}/${selectedItemFile.name}`;
            let iRefItem = storageRefItem.child(pathItem);
            iRefItem.put(selectedItemFile).then((snapshot) => {
                category.image = pathItem;

                let storageRef = firebase.storage().ref();
                let spaceRef = storageRef.child(category.image);

                console.log(category.image);
                storageRef.child(category.image).getDownloadURL().then((url) => {
                    // Set image url
                    console.log(url);


                    category.firebase_url = url;

                    return this.categories.push(category);

                }).catch((error) => {
                    console.log(error);
                });


                // return this.restaurants.push(restaurant);
            });
        }

        // return this.categories.push(category);


    }

    updateCategory(id, category) {
        //private items = this.db.database.list('listings');
        //items.update(category);
        return this.categories.update(id, category);

    }

    getCategories() {
        this.categories = this.db.list('/category') as AngularFireList<Category[]>
        return this.categories;
    }

    getCategoryDetails(cat_id) {
        this.category_details = this.db.object('/category/' + cat_id) as AngularFireObject<Category>
        return this.category_details;

    }


    deleteCategory(id) {
        //private items = this.db.database.list('listings');
        //items.remove(category);
        return this.categories.remove(id);

    }


    getItems() {
        return this.items;

    }

    addItem(item) {

        let storageRefItem = firebase.storage().ref();
        for (let selectedItemFile of [(<HTMLInputElement>document.getElementById('image')).files[0]]) {

            //let path = '/${this.folder}/${selectedFile.name}';
            let pathItem = `/${this.itemFolder}/${selectedItemFile.name}`;
            let iRefItem = storageRefItem.child(pathItem);
            iRefItem.put(selectedItemFile).then((snapshot) => {
                item.image = pathItem;

                let storageRef = firebase.storage().ref();
                let spaceRef = storageRef.child(item.image);

                console.log(item.image);
                storageRef.child(item.image).getDownloadURL().then((url) => {
                    // Set image url
                    console.log(url);


                    item.image_firebase_url = url;

                    return this.items.push(item);

                }).catch((error) => {
                    console.log(error);
                });

                //return this.items.push(item);
            });
        }

    }
    getItemDetails(id) {
        this.item_details = this.db.object('/items/' + id) as AngularFireObject<Item>
        return this.item_details;
    }
    updateItem(id, item) {
        //private items = this.db.database.list('listings');
        //items.update(category);
        return this.items.update(id, item);
    }

    deleteItem(id) {
        //private items = this.db.database.list('listings');
        //items.remove(category);
        return this.items.remove(id);

    }

    getAvailableRestaurants() {
        return this.db.list("/restaurants");
    }

    getVendor() {
        return this.db.list("/users",  ref => ref.orderByChild('is_vendor').equalTo(true));
        //     query: {
        //         orderByChild: "is_vendor",
        //         equalTo: true,
        //     }
        // });
    }

    addVendor(vendor) {
        return new Promise((resolve, reject) => {
            this.afh.auth.createUserWithEmailAndPassword(vendor.EmailAddress,
               vendor.Password
            ).then(res => {
                console.log(res);
                let usersInfo = firebase.database().ref("/users");
                usersInfo.child(res.user.uid).set({
                    email: vendor.EmailAddress,
                    displayName: vendor.FirstName,
                    lastName: vendor.LastName,
                    is_vendor: true,
                    facebook: false,
                    photoURL: "assets/images/person.png"
                }).then(userRes => {
                    console.log(userRes);
                }).catch(e => {
                    reject(e.message);
                });
                this.db.object("/restaurants/" + vendor.Restaurant).update({user_id: res.uid}).then(res => {
                    alert("Vendor Successfully Added");
                    resolve("Vendor Created Successfully");
                }).catch(e => {
                    reject(e.message);
                });
            }).catch(e => {
                reject(e.message);
            });
        });


        // }

    }


}


interface Listing {
    $key?: string;
    title?: string;
    type?: string;
    image?: string;
    city?: string;
    owner?: string;
    bedrooms?: string;
    path?: any;
}

interface Restaurant {
    $key?: string;
    address?: string;
    description?: string;
    image?: string;
    info?: string;
    lat?: string;
    long?: string;
    mark?: string;
    option?: string;
    outlet?: string;
    phonenumber?: string;
    title?: string;
    firebase_url?: string;
    img?: string;
}

interface Category {
    $key?: string;
    cat_id?: string;
    cat_name?: string;
    res_name?: string;
    image?: string;
    firebase_url?: string;
}


interface Item {
    $key?: string;
    available?: string;
    category?: string;
    description?: string;
    image?: string;
    name?: string;
    price?: string;
    stock?: string;
    categories?: string;
    percent?: string;
    image_firebase_url?: string;
}

interface Vendor {
    $key?: string;
    displayName?: string;
    email?: string;
    lastName?: string;
    facebook?: string;
    is_vendor?: string;
    photoURL?: string;
}

interface Order {
    $key?: string;
    address_id?: string;
    created?: string;
    item_qty?: string;
    order_date_time?: string;
    payment_id?: string;
    product_firebase?: string;
    product_id?: string;
    product_image?: string;
    product_price?: string;
    product_total_price?: string;
    restaurant_id?: string;
    restaurant_name?: string;
    status?: string;
    user_id?: string;
    user_name?: string;
    restaurant_owner_id?: string;
    checked?: string;
}
