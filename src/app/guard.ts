import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AngularFire } from 'angularfire2';

@Injectable()
export class RouterGuard implements CanActivate {

    constructor(private router: Router,private af: AngularFire,) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      
        // if(localStorage){
        //     var item = localStorage.getItem('p');
        //     if (item === '10') {
        //         return true;
        //     }
        // }
        // else   {
        //     console.log("No LocalStorage support");
        // } 
        // // not logged in so redirect to login page with the return url
        // this.router.navigate(['/']);
        // if (localStorage.getItem('p') !== '9' && localStorage.getItem('p') !== '10') alert("Please Login");
        // else alert("You're not admin");
        // return false;
          if(this.af.auth){
              return true;
          }
          else{

            this.router.navigate(['/login']);
            alert("You're not admin");
          }
return false;
    }
}
